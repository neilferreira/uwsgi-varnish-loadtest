```
docker build -f docker/varnish/Dockerfile -t loadtest-varnish:latest .
docker build -f docker/uwsgi/Dockerfile -t loadtest-uwsgi:latest .
```

# Run uWSGI
```
docker run -p 8080:8080 loadtest-uwsgi:latest
```
or locally
```
pip3 install uwsgi
uwsgi uwsgi.ini
```

# Run varnish
```
LOCAL_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -1)
docker run \
    -e BACKENDS=${LOCAL_IP}':8080' \
    -e ALLOWED_PURGE_CIDR_RANGES='"172.0.0.0"/8; "192.168.0.0"/8;' \
    -p 80:80 \
    loadtest-varnish:latest
```

# Outputs

this runs:
http://localhost:8080/api/ (uwsgi)
http://localhost:80/api/ (uwsgi varnish)

# Perform load test
python3 load_test.py


# Debugging
> docker ps

(look for the image id of varnish)
> docker exec -it [image_id] /bin/sh

> varnishlog

(perform load test)

In varnishlog:
CTRL + F "503"
Look for "RespStatus     503"

```
*   << Request  >> 165042
-   Begin          req 165041 rxreq
-   Timestamp      Start: 1565829963.366111 0.000000 0.000000
-   Timestamp      Req: 1565829963.366111 0.000000 0.000000
-   VCL_use        boot
-   ReqStart       172.17.0.1 37302 a0
-   ReqMethod      POST
-   ReqURL         /api
-   ReqProtocol    HTTP/1.1
-   ReqHeader      Host: localhost
-   ReqHeader      User-Agent: python-requests/2.18.4
-   ReqHeader      Accept-Encoding: gzip, deflate
-   ReqHeader      Accept: */*
-   ReqHeader      Connection: keep-alive
-   ReqHeader      Content-Length: 0
-   ReqHeader      X-Forwarded-For: 172.17.0.1
-   VCL_call       RECV
-   VCL_return     pass
-   VCL_call       HASH
-   VCL_return     lookup
-   VCL_call       PASS
-   VCL_return     fetch
-   Link           bereq 165043 pass
-   Timestamp      Fetch: 1565829963.366220 0.000110 0.000110
-   RespProtocol   HTTP/1.1
-   RespStatus     503
-   RespReason     Backend fetch failed
-   RespHeader     Date: Thu, 15 Aug 2019 00:46:03 GMT
-   RespHeader     Server: Varnish
-   RespHeader     content-type: text/html; charset=utf-8
-   RespHeader     Retry-After: 5
-   RespHeader     X-Varnish: 165042
-   RespHeader     Age: 0
-   RespHeader     Via: 1.1 varnish (Varnish/6.2)
-   VCL_call       DELIVER
-   RespHeader     X-Cache: MISS
-   VCL_return     deliver
-   Timestamp      Process: 1565829963.366225 0.000115 0.000005
-   Filters
-   RespHeader     Content-Length: 283
-   RespHeader     Connection: keep-alive
-   Timestamp      Resp: 1565829963.366244 0.000133 0.000018
-   ReqAcct        163 0 163 266 283 549
-   End
```

Look for the matching backend request

> Link           bereq 165043 pass

```
*   << BeReq    >> 165043
-   Begin          bereq 165042 pass
-   VCL_use        boot
-   Timestamp      Start: 1565829963.366134 0.000000 0.000000
-   BereqMethod    POST
-   BereqURL       /api
-   BereqProtocol  HTTP/1.1
-   BereqHeader    Host: localhost
-   BereqHeader    User-Agent: python-requests/2.18.4
-   BereqHeader    Accept-Encoding: gzip, deflate
-   BereqHeader    Accept: */*
-   BereqHeader    Content-Length: 0
-   BereqHeader    X-Forwarded-For: 172.17.0.1
-   BereqHeader    X-Varnish: 165043
-   VCL_call       BACKEND_FETCH
-   VCL_return     fetch
-   BackendOpen    21 server1 172.17.0.1 8080 172.17.0.3 44478
-   BackendStart   172.17.0.1 8080
-   Timestamp      Bereq: 1565829963.366170 0.000036 0.000036
-   FetchError     HTC eof (-1)
-   BackendClose   21 server1
-   Timestamp      Beresp: 1565829963.366190 0.000056 0.000020
-   Timestamp      Error: 1565829963.366192 0.000058 0.000002
-   BerespProtocol HTTP/1.1
-   BerespStatus   503
-   BerespReason   Service Unavailable
-   BerespReason   Backend fetch failed
-   BerespHeader   Date: Thu, 15 Aug 2019 00:46:03 GMT
-   BerespHeader   Server: Varnish
-   VCL_call       BACKEND_ERROR
-   BerespHeader   content-type: text/html; charset=utf-8
-   BerespHeader   Retry-After: 5
-   VCL_return     deliver
-   Storage        malloc Transient
-   Length         283
-   BereqAcct      187 0 187 0 0 0
-   End
```

