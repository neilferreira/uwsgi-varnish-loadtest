import requests
from multiprocessing import Process

# LOAD TEST SETTINGS
"""
Set processing_threads for how many simultaneous logins you would like to
process
Set logins_per_thread to extend the duration of the test
"""

processing_threads = 6
logins_per_thread = 300


def worker(port):  # the managed list `L` passed explicitly.
    for x in range(0, logins_per_thread):
        response = requests.request('POST', f'http://localhost:{port}/api')
        if 'token' not in response.text:
            # This is a fail
            print(response.content)


if __name__ == '__main__':
    services = {
        'uwsgi': 8080,
        'uwsgi_varnish': 80,
    }
    for service in services:
        print("\n\n\n")
        print(f'Doing load test on {service}')
        print('Seeing any output means this was a fail.')
        port = services[service]
        processes = []
        for i in range(processing_threads):
            p = Process(target=worker, args=(port, ))  # Passing the list
            p.start()
            processes.append(p)
        for p in processes:
            p.join()
