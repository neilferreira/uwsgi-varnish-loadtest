import os
from mako.template import Template


# Input variables for template replacement
variables = {
  'backends': [backend.strip().split(':') for backend in
               os.environ['BACKENDS'].split(',')],
  'allowed_purge_cir_ranges': os.environ['ALLOWED_PURGE_CIDR_RANGES']
}

# Read the template file, and render the new configuration
template_file = '/tmp/default.vcl.tmpl'
with open(template_file, 'r') as file:
    filedata = Template(file.read()).render(**variables)

# Write the VCL file to disk
with open('/etc/varnish/default.vcl', 'w') as file:
    file.write(filedata)
