#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;

import directors;

% for backend_host, backend_port in backends:
backend server${loop.index + 1} {
    .host = "${backend_host}";
    .port = "${backend_port}";
}
% endfor

acl purge {
  # ACL we'll use later to allow purges
  ${allowed_purge_cir_ranges}
}

sub vcl_init {
    new bar = directors.round_robin();
    % for backend_host, backend_port in backends:
    bar.add_backend(server${loop.index + 1});
    % endfor
}

sub vcl_recv {
    # send all traffic to the bar director:
    set req.backend_hint = bar.backend();

    # allow PURGE only from the purge acl
    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return(synth(405, "Not allowed."));
        }
        return (purge);
    }
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.
}

sub vcl_backend_response {
    # Enable gzip compression for all our views which return json
    if (beresp.http.content-type ~ "application/json") {
        set beresp.do_gzip = true;
    }
}

sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.

    # Remove the X-Involved-Tags header from the response, which is used
    # internally for cache purging
    unset resp.http.X-Involved-Tags;

    # Add a header to the response to indicate if the request was a hit or miss
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
    }
    else {
        set resp.http.X-Cache = "MISS";
    }
}
